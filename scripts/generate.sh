#!/usr/bin/env bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
BASE_DIR="$SCRIPT_DIR/.."
SRC_DIR="$BASE_DIR/src"
GEN_DIR="$BASE_DIR/gen"

java -jar $BASE_DIR/swagger-codegen-cli*.jar generate -i $SRC_DIR/swagger.yml -l typescript-fetch -o $GEN_DIR -c $BASE_DIR/config.json

rm $GEN_DIR/LICENSE # Should be LICENCE, but we've want the MIT licence anyway.

cp -R $SRC_DIR/static/* $GEN_DIR
cp $SRC_DIR/ts/index.ts $GEN_DIR

if which node > /dev/null; then
  node $SCRIPT_DIR/update-generated-code.js
  cd gen
  npm install
  npm run prepublish
fi
