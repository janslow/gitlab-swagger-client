#!/usr/bin/env node

const path = require('path');
const fs = require('fs');

const genDir = path.resolve(__dirname, '../gen/');
const pkgPath = path.resolve(genDir, 'package.json');

const pkg = JSON.parse(fs.readFileSync(pkgPath).toString());

Object.assign(pkg, {
  description: 'A fetch-based GitLab API client, auto-generated using Swagger codegen.',
  keywords: ['GitLab', 'API', 'fetch', 'promise'],
  homepage: 'https://gitlab.com/janslow/gitlab-swagger-client',
  bugs: 'https://gitlab.com/janslow/gitlab-swagger-client/issues',
  license: 'MIT',
  author: {
    name: 'Jay Anslow',
    email: 'jay@anslow.me.uk',
  },
  files: [
    'dist',
    'typings.json',
  ],
  main: './dist/index.js',
  browser: './dist/index.js',
  typings: './dist/index.d.ts',
  repository: {
    type: 'git',
    url: 'https://gitlab.com/janslow/gitlab-swagger-client.git',
  },
  scripts: Object.assign(pkg.scripts, {
    test: 'echo "Currently no tests :("',
  }),

})

fs.writeFileSync(pkgPath, JSON.stringify(pkg, undefined, 2));
