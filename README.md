# GitLab Swagger Client

An API client for the [GitLab API](http://docs.gitlab.com/ce/api/) generated from a Swagger specification.

At the moment, only a fetch-based TypeScript client is generated. It would be nice to produce other clients in the future.

## Clients
### TypeScript

See the [package README](src/static/README.md) for usage/installation information.

## Development
### Changing API methods
The API is defined using the [Swagger 2.0/OpenAPI specification](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md).

To add methods, fix existing ones etc., you should edit `src/swagger.yml` and the rest will be done by the build process.

### Automatic builds
Enable GitLab CI and add at least one runner to build this project in GitLab. It will produce a NPM package tarball, including TypeScript definitions, which can be used in other packages.

### Local development.
In an environment with Java (tested with `java@8`) and Node/NPM (tested with `node@6`, `node@4` and `npm@3`), rune `./scripts/generate.sh`.

This will run `swagger-codegen-cli` to produce an NPM project, and compile it in the `gen` directory.This will run `swagger-codegen-cli` to produce an NPM project, and compile it in the `gen` directory.

You can then run `npm link` in the `gen` directory to allow use in other packages (using `npm link`). See the [`npm link` docs](https://docs.npmjs.com/cli/link) for more information.
