# Changelog

## 0.1.1
* Fix issue when there are no headers passed to the `authorizeFetch` method in the wrapped API classes. (#2)

## 0.1.0
* Initial Release
* Partial implementations of the following APIs:
  - Groups
  - Projects
  - Tags
  - Merge Requests
