import * as isomorphicFetch from "isomorphic-fetch";

import { BaseAPI, FetchAPI } from "./api";

export * from "./api";

export type GitLabAuth = GitLabPrivateTokenAuth | GitLabOAuth2TokenAuth;

export interface GitLabPrivateTokenAuth {
  privateToken: string;
}
function isGitLabPrivateTokenAuth(maybe: GitLabAuth): maybe is GitLabPrivateTokenAuth {
  return typeof (<GitLabPrivateTokenAuth> maybe).privateToken === "string";
}

export interface GitLabOAuth2TokenAuth {
  bearerToken: string;
}
function isGitLabOAuth2TokenAuth(maybe: GitLabAuth): maybe is GitLabOAuth2TokenAuth {
  return typeof (<GitLabOAuth2TokenAuth> maybe).bearerToken === "string";
}

function authorizeFetch(fetch: FetchAPI, auth: GitLabAuth): FetchAPI {
  return (url: string, init?: any): Promise<any> => {
    init = Object.assign({}, init, {
      headers: Object.assign({}, init.headers),
    });
    if (isGitLabPrivateTokenAuth(auth)) {
      init.headers["PRIVATE-TOKEN"] = auth.privateToken;
    }
    else if (isGitLabOAuth2TokenAuth(auth)) {
      init.headers["Authorization"] = `Bearer ${auth.bearerToken}`;
    }
    return fetch(url, init);
  };
}

import { GroupsApi as OriginalGroupsApi } from './api';
export class GroupsApi extends OriginalGroupsApi {
  constructor(auth: GitLabAuth, fetch: FetchAPI = isomorphicFetch, basePath?: string) {
    super(authorizeFetch(fetch, auth), basePath);
  }
}

import { MergeRequestsApi as OriginalMergeRequestsApi } from './api';
export class MergeRequestsApi extends OriginalMergeRequestsApi {
  constructor(auth: GitLabAuth, fetch: FetchAPI = isomorphicFetch, basePath?: string) {
    super(authorizeFetch(fetch, auth), basePath);
  }
}

import { ProjectApi as OriginalProjectApi } from './api';
export class ProjectApi extends OriginalProjectApi {
  constructor(auth: GitLabAuth, fetch: FetchAPI = isomorphicFetch, basePath?: string) {
    super(authorizeFetch(fetch, auth), basePath);
  }
}

import { TagsApi as OriginalTagsApi } from './api';
export class TagsApi extends OriginalTagsApi {
  constructor(auth: GitLabAuth, fetch: FetchAPI = isomorphicFetch, basePath?: string) {
    super(authorizeFetch(fetch, auth), basePath);
  }
}
