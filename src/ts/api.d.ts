export interface FetchAPI {
    (url: string, init?: any): Promise<any>;
}

export declare class BaseAPI {
    basePath: string;
    fetch: FetchAPI;
    constructor(fetch?: FetchAPI, basePath?: string);
}

export declare class GroupsApi extends BaseAPI {}

export declare class MergeRequestsApi extends BaseAPI {}

export declare class ProjectApi extends BaseAPI {}

export declare class TagsApi extends BaseAPI {}
